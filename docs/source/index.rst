=============
i3dxrd
=============

Project intended to replace the ImageD11 interface, using the ImageD11 core functions.
A class Dataset is created to initialise the data and apply any needed functionalities.
All the widgets are created using silx and Qt. To define the workflow an Orange3 add-on is
implemented.

For more about ImageD11 please visit: https://i3dxrd.readthedocs.io/en/latest/

.. toctree::
   :hidden:

   api.rst
   getting_started.rst
   install.rst
   changelog.rst
   license.rst

:doc:`install`
    How to install *i3dxrd* on Linux

:doc:`getting_started`
    How to run a *i3dxrd* workflow

:doc:`changelog`
    List of changes between releases

:doc:`license`
    License and copyright information

:doc:`api`
    API


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

