API
---

Dataset
=======

.. automodule:: i3dxrd.dataset
    :members:

data selection
==============

.. automodule:: i3dxrd.data_selection
    :members:

GUI
===
.. automodule:: i3dxrd.gui.dataSelectionWidget
    :members:

.. automodule:: i3dxrd.gui.backgroundSubtractionWidget
    :members:

.. automodule:: i3dxrd.gui.stackViewWidget
    :members:

.. automodule:: i3dxrd.gui.peakSearchWidget
    :members:

.. automodule:: i3dxrd.gui.showPeaksWidget
    :members:

.. automodule:: i3dxrd.gui.calibrationWidget
    :members:

.. automodule:: i3dxrd.gui.indexingWidget
    :members:

.. automodule:: i3dxrd.gui.makemapWidget
    :members:

.. automodule:: i3dxrd.gui.plotMapWidget
    :members:
